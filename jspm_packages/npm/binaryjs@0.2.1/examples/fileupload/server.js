/* */ 
var fs = require('fs');
var http = require('http');
var express = require('express');
var app = express();
app.use(express.static(__dirname + '/public'));
var server = http.createServer(app);
var BinaryServer = require('../../lib/server').BinaryServer;
var bs = BinaryServer({server: server});
bs.on('connection', function(client) {
  client.on('stream', function(stream, meta) {
    var file = fs.createWriteStream(__dirname + '/public/' + meta.name);
    stream.pipe(file);
    stream.on('data', function(data) {
      stream.write({rx: data.length / meta.size});
    });
  });
});
server.listen(9000);
console.log('HTTP and BinaryJS server started on port 9000');
