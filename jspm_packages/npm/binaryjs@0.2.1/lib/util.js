/* */ 
(function(process) {
  var BinaryPack = require('binarypack');
  var util = {
    inherits: function(ctor, superCtor) {
      ctor.super_ = superCtor;
      ctor.prototype = Object.create(superCtor.prototype, {constructor: {
          value: ctor,
          enumerable: false,
          writable: true,
          configurable: true
        }});
    },
    extend: function(dest, source) {
      for (var key in source) {
        if (source.hasOwnProperty(key)) {
          dest[key] = source[key];
        }
      }
      return dest;
    },
    pack: BinaryPack.pack,
    unpack: BinaryPack.unpack,
    isNode: true,
    setZeroTimeout: (function(global) {
      return process.nextTick;
      var timeouts = [];
      var messageName = 'zero-timeout-message';
      function setZeroTimeoutPostMessage(fn) {
        timeouts.push(fn);
        global.postMessage(messageName, '*');
      }
      function handleMessage(event) {
        if (event.source == global && event.data == messageName) {
          if (event.stopPropagation) {
            event.stopPropagation();
          }
          if (timeouts.length) {
            timeouts.shift()();
          }
        }
      }
      if (global.addEventListener) {
        global.addEventListener('message', handleMessage, true);
      } else if (global.attachEvent) {
        global.attachEvent('onmessage', handleMessage);
      }
      return setZeroTimeoutPostMessage;
    }(this))
  };
  module.exports = util;
})(require('process'));
