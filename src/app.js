import {semanticUI} from 'semantic-ui';

export class App {
  configureRouter(config, router) {
    config.title = 'Aurelia';
    config.map([
      { route: ['', 'welcome'], name: 'welcome', moduleId: './welcome', nav: true, title: 'Welcome' },
      { route: 'users', name: 'users', moduleId: './users', nav: true, title: 'Github Users' },
      { route: 'user_profile',  name: 'user_profile',  moduleId: './user_profile',  nav: true, title:'user_profile' },
      { route: 'client',  name: 'client',  moduleId: './client',  nav: true, title:'client' }
    ]);

    this.router = router;
  }

  showSidebar() {
    $('#toc').sidebar({
      dimPage: false,
      transition: 'overlay',
      mobileTransition: 'uncover'
    }
    ).sidebar('toggle');
  }

  showModal() {
    $('.modal').modal('show');
  }
}
