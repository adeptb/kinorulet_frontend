import base64 from 'base64-js/lib/b64.js'
import {WSData} from './model/WSData'

export class Welcome {

    video = null;
    audio = null;
    canvas = null;
    ctx = null;
    localMediaStream = null;
    client = null;
    stream_write = null;
    stream_read = null;
    quality = 0.75;

    attached() {
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
        this.audio = new AudioContext();
        this.video = document.getElementById("video");
        this.canvas = document.querySelector('canvas');
        this.ctx = this.canvas.getContext('2d');
        this.client = new WebSocket('ws://localhost:8080/greeting');
        // this.client.binaryType = 'arraybuffer';
        var videoObj = {
            "video": true,
            "audio": true
        };
        var errBack = function(error) {
            console.log("Video capture error: ", error.code);
        };
        this.client.onopen = () => {
            if (navigator.getUserMedia) {
                navigator.getUserMedia(videoObj, (stream) => {
                    // var input = this.audio.createMediaStreamSource(stream);
                    // var recordNode = this.audio.createScriptProcessor(4096);
                    // recordNode.onaudioprocess = (e) => {
                    //   var buffer = e.inputBuffer.getChannelData(0);
                    //   this.client.send(JSON.stringify(new WSData(buffer, WSData.audioType).toJSON()));
                    // };
                    // input.connect(recordNode);
                    // recordNode.connect(this.audio.destination);
                    video.src = window.URL.createObjectURL(stream);
                    video.play();
                }, errBack);
            }
            setInterval(() => {
                this.sendSnapshot()
            }, 40);
        }
    }

    sendSnapshot() {
        if (this.video) {
            this.ctx.drawImage(this.video, 0, 0, video.width, video.height);
            var imgSrc = this.canvas.toDataURL("image/jpeg", parseFloat(this.quality));
            this.client.send(JSON.stringify(new WSData(imgSrc.replace("data:image/jpeg;base64,", ""), WSData.videoType).toJSON()));
        }
    }
}
