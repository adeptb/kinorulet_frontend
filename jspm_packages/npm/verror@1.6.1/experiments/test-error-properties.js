/* */ 
(function(process) {
  var VError = require('../lib/verror');
  var errorname = 'OtherName';
  var message = 'my sample error';
  var omessage = 'other error message';
  function main() {
    console.log('node %s', process.version);
    runBattery(Error);
    console.log('===============================');
    runBattery(VError);
  }
  function runBattery(klass) {
    var name,
        error;
    name = klass.name;
    error = new klass(message);
    printErrorInfo(name, 'simple', error);
    error.name = errorname;
    printErrorInfo(name, 'changed "name" after fetching stack', error);
    error = new klass(message);
    error.name = errorname;
    printErrorInfo(name, 'changed "name" before fetching stack', error);
    error = new klass(message);
    error.message = omessage;
    printErrorInfo(name, 'changed "message" before fetching stack', error);
    error.message = message;
    printErrorInfo(name, 'changed "message" back after fetching stack', error);
    error = new klass(message);
    error.otherprop = 'otherpropvalue';
    printErrorInfo(name, 'with "otherprop" property', error);
  }
  function printErrorInfo(classname, label, err) {
    console.log('------------------');
    console.log('test: %s, %s', classname, label);
    console.log('instanceof Error: %s', err instanceof Error);
    console.log('constructor: %s', err.constructor.name);
    console.log('error name: %s', err.name);
    console.log('error message: %s', err.message);
    console.log('error toString: %s', err.toString());
    console.log('has "otherprop": %s', err.hasOwnProperty('otherprop'));
    console.log('error stack: %s', err.stack);
    console.log('inspect: ', err);
  }
  main();
})(require('process'));
