/* */ 
(function(Buffer) {
  getBufferFromHexString = function(byteStr) {
    var bytes = byteStr.split(' ');
    var buf = new Buffer(bytes.length);
    for (var i = 0; i < bytes.length; ++i) {
      buf[i] = parseInt(bytes[i], 16);
    }
    return buf;
  };
  getHexStringFromBuffer = function(data) {
    var s = '';
    for (var i = 0; i < data.length; ++i) {
      s += padl(data[i].toString(16), 2, '0') + ' ';
    }
    return s.trim();
  };
  splitBuffer = function(buffer) {
    var b1 = new Buffer(Math.ceil(buffer.length / 2));
    buffer.copy(b1, 0, 0, b1.length);
    var b2 = new Buffer(Math.floor(buffer.length / 2));
    buffer.copy(b2, 0, b1.length, b1.length + b2.length);
    return [b1, b2];
  };
  mask = function(buf, maskString) {
    if (typeof buf == 'string')
      buf = new Buffer(buf);
    var mask = getBufferFromHexString(maskString || '34 83 a8 68');
    for (var i = 0; i < buf.length; ++i) {
      buf[i] ^= mask[i % 4];
    }
    return buf;
  };
  getHybiLengthAsHexString = function(len, masked) {
    if (len < 126) {
      var buf = new Buffer(1);
      buf[0] = (masked ? 0x80 : 0) | len;
    } else if (len < 65536) {
      var buf = new Buffer(3);
      buf[0] = (masked ? 0x80 : 0) | 126;
      getBufferFromHexString(pack(4, len)).copy(buf, 1);
    } else {
      var buf = new Buffer(9);
      buf[0] = (masked ? 0x80 : 0) | 127;
      getBufferFromHexString(pack(16, len)).copy(buf, 1);
    }
    return getHexStringFromBuffer(buf);
  };
  unpack = function(buffer) {
    var n = 0;
    for (var i = 0; i < buffer.length; ++i) {
      n = (i == 0) ? buffer[i] : (n * 256) + buffer[i];
    }
    return n;
  };
  pack = function(length, number) {
    return padl(number.toString(16), length, '0').replace(/([0-9a-f][0-9a-f])/gi, '$1 ').trim();
  };
  padl = function(s, n, c) {
    return new Array(1 + n - s.length).join(c) + s;
  };
})(require('buffer').Buffer);
