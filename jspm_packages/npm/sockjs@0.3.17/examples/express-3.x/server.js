/* */ 
var express = require('express');
var sockjs = require('../../index');
var http = require('http');
var sockjs_opts = {sockjs_url: "http://cdn.jsdelivr.net/sockjs/1.0.1/sockjs.min.js"};
var sockjs_echo = sockjs.createServer(sockjs_opts);
sockjs_echo.on('connection', function(conn) {
  conn.on('data', function(message) {
    conn.write(message);
  });
});
var app = express();
var server = http.createServer(app);
sockjs_echo.installHandlers(server, {prefix: '/echo'});
console.log(' [*] Listening on 0.0.0.0:9999');
server.listen(9999, '0.0.0.0');
app.get('/', function(req, res) {
  res.sendfile(__dirname + '/index.html');
});
