/* */ 
(function(Buffer) {
  var benchmark = require('benchmark'),
      Sender = require('../index').Sender,
      suite = new benchmark.Suite('Sender');
  require('tinycolor');
  require('./util');
  suite.on('start', function() {
    sender = new Sender();
    sender._socket = {write: function() {}};
  });
  suite.on('cycle', function() {
    sender = new Sender();
    sender._socket = {write: function() {}};
  });
  framePacket = new Buffer(200 * 1024);
  framePacket.fill(99);
  suite.add('frameAndSend, unmasked (200 kB)', function() {
    sender.frameAndSend(0x2, framePacket, true, false);
  });
  suite.add('frameAndSend, masked (200 kB)', function() {
    sender.frameAndSend(0x2, framePacket, true, true);
  });
  suite.on('cycle', function(bench, details) {
    console.log('\n  ' + suite.name.grey, details.name.white.bold);
    console.log('  ' + [details.hz.toFixed(2).cyan + ' ops/sec'.grey, details.count.toString().white + ' times executed'.grey, 'benchmark took '.grey + details.times.elapsed.toString().white + ' sec.'.grey].join(', '.grey));
  });
  if (!module.parent) {
    suite.run();
  } else {
    module.exports = suite;
  }
})(require('buffer').Buffer);
