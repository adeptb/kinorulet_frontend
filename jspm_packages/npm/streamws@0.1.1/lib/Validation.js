/* */ 
try {
  module.exports = require('../build/Release/validation');
} catch (e) {
  try {
    module.exports = require('../build/default/validation');
  } catch (e) {
    try {
      module.exports = require('./Validation.fallback');
    } catch (e) {
      console.error('validation.node seems to not have been built. Run npm install.');
      throw e;
    }
  }
}
