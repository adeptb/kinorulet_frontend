/* */ 
(function(process) {
  this.Context = function(vow, ctx, env) {
    var that = this;
    this.tests = vow.callback;
    this.topics = (ctx.topics || []).slice(0);
    this.emitter = null;
    this.env = env || {};
    this.env.context = this;
    this.env.callback = function() {
      var ctx = this;
      var args = Array.prototype.slice.call(arguments);
      var emit = (function(args) {
        if (vow.batch.suite.options.error) {
          return function() {
            var e = args.shift();
            that.emitter.ctx = ctx;
            if (typeof(e) === 'boolean' && args.length === 0) {
              that.emitter.emit.call(that.emitter, 'success', e);
            } else {
              if (e) {
                that.emitter.emit.apply(that.emitter, ['error', e].concat(args));
              } else {
                that.emitter.emit.apply(that.emitter, ['success'].concat(args));
              }
            }
          };
        } else {
          return function() {
            that.emitter.ctx = ctx;
            that.emitter.emit.apply(that.emitter, ['success'].concat(args));
          };
        }
      })(args.slice(0));
      if (that.emitter) {
        emit();
      } else {
        process.nextTick(emit);
      }
    };
    this.name = vow.description;
    if (this.name === 'events') {
      this.name = vow.description = 'on';
    }
    if (this.name === 'on' && ctx.isEvent) {
      this.after = ctx.name;
    }
    if (ctx.name === 'on') {
      this.isEvent = true;
      this.event = this.name;
      this.after = ctx.after;
    } else {
      this.isEvent = false;
      this.event = 'success';
    }
    this.title = [ctx.title || '', vow.description || ''].join(/^[#.:]/.test(vow.description) ? '' : ' ').trim();
  };
})(require('process'));
