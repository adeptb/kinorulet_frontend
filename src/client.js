import base64 from 'base64-js/lib/b64.js'

export class Client {
    canvas = null;
    ctx = null;

    attached() {

      this.canvas = document.querySelector('canvas');
      this.ctx = this.canvas.getContext('2d');
      this.client = new WebSocket('ws://localhost:8080/greeting');
      this.client.binaryType = 'arraybuffer';
      this.client.onopen = () => {
          console.log("open")
      }
      var ctx = this.ctx;

      this.client.onmessage = (e) => {
          var base64string = base64.fromByteArray(new Uint8Array(e.data));
          var image = new Image();
          image.src = "data:image/jpeg;base64," + base64string;
          image.onload = function() {
              ctx.drawImage(image, 0, 0, 640, 480);
          };

      }
    }
}
