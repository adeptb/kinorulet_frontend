/* */ 
(function(process) {
  var mod_assert = require('assert');
  var mod_util = require('util');
  var mod_extsprintf = require('extsprintf');
  var mod_isError = require('core-util-is').isError;
  module.exports = VError;
  VError.VError = VError;
  VError.SError = SError;
  VError.WError = WError;
  VError.MultiError = MultiError;
  function VError(options) {
    var args,
        obj,
        causedBy,
        ctor,
        tailmsg;
    if (!(this instanceof VError)) {
      args = Array.prototype.slice.call(arguments, 0);
      obj = Object.create(VError.prototype);
      VError.apply(obj, arguments);
      return (obj);
    }
    if (mod_isError(options) || typeof(options) === 'object') {
      args = Array.prototype.slice.call(arguments, 1);
    } else {
      args = Array.prototype.slice.call(arguments, 0);
      options = undefined;
    }
    if (!options || !options.strict) {
      args = args.map(function(a) {
        return (a === null ? 'null' : a === undefined ? 'undefined' : a);
      });
    }
    tailmsg = args.length > 0 ? mod_extsprintf.sprintf.apply(null, args) : '';
    this.jse_shortmsg = tailmsg;
    this.jse_summary = tailmsg;
    if (options) {
      causedBy = options.cause;
      if (!causedBy || !mod_isError(options.cause))
        causedBy = options;
      if (causedBy && mod_isError(causedBy)) {
        this.jse_cause = causedBy;
        this.jse_summary += ': ' + causedBy.message;
      }
    }
    this.message = this.jse_summary;
    Error.call(this, this.jse_summary);
    if (Error.captureStackTrace) {
      ctor = options ? options.constructorOpt : undefined;
      ctor = ctor || arguments.callee;
      Error.captureStackTrace(this, ctor);
    }
    return (this);
  }
  mod_util.inherits(VError, Error);
  VError.prototype.name = 'VError';
  VError.prototype.toString = function ve_toString() {
    var str = (this.hasOwnProperty('name') && this.name || this.constructor.name || this.constructor.prototype.name);
    if (this.message)
      str += ': ' + this.message;
    return (str);
  };
  VError.prototype.cause = function ve_cause() {
    return (this.jse_cause);
  };
  function SError() {
    var fmtargs,
        opts,
        key,
        args;
    opts = {};
    opts.constructorOpt = SError;
    if (mod_isError(arguments[0])) {
      opts.cause = arguments[0];
      fmtargs = Array.prototype.slice.call(arguments, 1);
    } else if (typeof(arguments[0]) == 'object') {
      for (key in arguments[0])
        opts[key] = arguments[0][key];
      fmtargs = Array.prototype.slice.call(arguments, 1);
    } else {
      fmtargs = Array.prototype.slice.call(arguments, 0);
    }
    opts.strict = true;
    args = [opts].concat(fmtargs);
    VError.apply(this, args);
  }
  mod_util.inherits(SError, VError);
  function MultiError(errors) {
    mod_assert.ok(errors.length > 0);
    this.ase_errors = errors;
    VError.call(this, errors[0], 'first of %d error%s', errors.length, errors.length == 1 ? '' : 's');
  }
  mod_util.inherits(MultiError, VError);
  function WError(options) {
    Error.call(this);
    var args,
        cause,
        ctor;
    if (typeof(options) === 'object') {
      args = Array.prototype.slice.call(arguments, 1);
    } else {
      args = Array.prototype.slice.call(arguments, 0);
      options = undefined;
    }
    if (args.length > 0) {
      this.message = mod_extsprintf.sprintf.apply(null, args);
    } else {
      this.message = '';
    }
    if (options) {
      if (mod_isError(options)) {
        cause = options;
      } else {
        cause = options.cause;
        ctor = options.constructorOpt;
      }
    }
    Error.captureStackTrace(this, ctor || this.constructor);
    if (cause)
      this.cause(cause);
  }
  mod_util.inherits(WError, Error);
  WError.prototype.name = 'WError';
  WError.prototype.toString = function we_toString() {
    var str = (this.hasOwnProperty('name') && this.name || this.constructor.name || this.constructor.prototype.name);
    if (this.message)
      str += ': ' + this.message;
    if (this.we_cause && this.we_cause.message)
      str += '; caused by ' + this.we_cause.toString();
    return (str);
  };
  WError.prototype.cause = function we_cause(c) {
    if (mod_isError(c))
      this.we_cause = c;
    return (this.we_cause);
  };
})(require('process'));
