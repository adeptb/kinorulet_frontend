/* */ 
(function(process) {
  var fs = require('fs'),
      package = JSON.parse(fs.readFileSync(__dirname + '/../package.json')),
      uglify = require('uglify-js');
  var template = '/*! binary.%ext% build:' + package.version + ', %type%. Copyright(c) 2012 Eric Zhang <eric@ericzhang.com> MIT Licensed */',
      prefix = '\n(function(exports){\n',
      development = template.replace('%type%', 'development').replace('%ext%', 'js'),
      production = template.replace('%type%', 'production').replace('%ext%', 'min.js'),
      suffix = '\n})(this);\n';
  var starttagIF = '// if node',
      endtagIF = '// end node';
  var base = ['../deps/js-binarypack/lib/bufferbuilder.js', '../deps/js-binarypack/lib/binarypack.js', '../deps/EventEmitter/EventEmitter.js', 'util.js', 'client/stream.js', 'client/blob_stream.js', 'stream.js', 'client.js'];
  var builder = module.exports = function() {
    var options,
        callback,
        error = null,
        args = Array.prototype.slice.call(arguments, 0),
        settings = {
          minify: true,
          node: false,
          custom: []
        };
    args.forEach(function(arg) {
      var type = Object.prototype.toString.call(arg).replace(/\[object\s(\w+)\]/gi, '$1').toLowerCase();
      switch (type) {
        case 'object':
          return options = arg;
        case 'function':
          return callback = arg;
      }
    });
    options = options || {};
    for (var option in options) {
      settings[option] = options[option];
    }
    var files = [];
    base.forEach(function(file) {
      files.push(__dirname + '/../lib/' + file);
    });
    var results = {};
    files.forEach(function(file) {
      fs.readFile(file, function(err, content) {
        if (err)
          error = err;
        results[file] = content;
        if (Object.keys(results).length !== files.length)
          return;
        var code = development,
            ignore = 0;
        code += prefix;
        files.forEach(function(file) {
          code += results[file];
        });
        if (settings.custom.length) {
          settings.custom.forEach(function(content) {
            code += content;
          });
        }
        if (!settings.node) {
          code = code.split('\n').filter(function(line) {
            var start = line.indexOf(starttagIF) >= 0,
                end = line.indexOf(endtagIF) >= 0,
                ret = ignore;
            if (start) {
              ignore++;
              ret = ignore;
            }
            if (end) {
              ignore--;
            }
            return ret == 0;
          }).join('\n');
        }
        code += suffix;
        if (settings.minify) {
          var ast = uglify.parser.parse(code);
          ast = uglify.uglify.ast_mangle(ast);
          ast = uglify.uglify.ast_squeeze(ast);
          code = production + uglify.uglify.gen_code(ast, {ascii_only: true});
        }
        callback(error, code);
      });
    });
  };
  builder.version = package.version;
  if (!module.parent) {
    var args = process.argv.slice(2);
    builder(args.length ? args : false, {minify: false}, function(err, content) {
      if (err)
        return console.error(err);
      console.log(__dirname);
      fs.write(fs.openSync(__dirname + '/../dist/binary.js', 'w'), content, 0, 'utf8');
      console.log('Successfully generated the development build: binary.js');
    });
    builder(args.length ? args : false, function(err, content) {
      if (err)
        return console.error(err);
      fs.write(fs.openSync(__dirname + '/../dist/binary.min.js', 'w'), content, 0, 'utf8');
      console.log('Successfully generated the production build: binary.min.js');
    });
  }
})(require('process'));
