/* */ 
(function(Buffer) {
  var util = require('util');
  var EMPTY = 0,
      BODY = 1;
  function Receiver() {
    this.state = EMPTY;
    this.buffers = [];
    this.messageEnd = -1;
    this.spanLength = 0;
    this.dead = false;
    this.onerror = function() {};
    this.ontext = function() {};
    this.onbinary = function() {};
    this.onclose = function() {};
    this.onping = function() {};
    this.onpong = function() {};
  }
  module.exports = Receiver;
  Receiver.prototype.add = function(data) {
    var self = this;
    function doAdd() {
      if (self.state === EMPTY) {
        if (data.length == 2 && data[0] == 0xFF && data[1] == 0x00) {
          self.reset();
          self.onclose();
          return;
        }
        if (data[0] !== 0x00) {
          self.error('payload must start with 0x00 byte', true);
          return;
        }
        data = data.slice(1);
        self.state = BODY;
      }
      self.buffers.push(data);
      if ((self.messageEnd = bufferIndex(data, 0xFF)) != -1) {
        self.spanLength += self.messageEnd;
        return self.parse();
      } else
        self.spanLength += data.length;
    }
    while (data)
      data = doAdd();
  };
  Receiver.prototype.cleanup = function() {
    this.dead = true;
    this.state = EMPTY;
    this.buffers = [];
  };
  Receiver.prototype.parse = function() {
    var output = new Buffer(this.spanLength);
    var outputIndex = 0;
    for (var bi = 0,
        bl = this.buffers.length; bi < bl - 1; ++bi) {
      var buffer = this.buffers[bi];
      buffer.copy(output, outputIndex);
      outputIndex += buffer.length;
    }
    var lastBuffer = this.buffers[this.buffers.length - 1];
    if (this.messageEnd > 0)
      lastBuffer.copy(output, outputIndex, 0, this.messageEnd);
    var tail = null;
    if (this.messageEnd < lastBuffer.length - 1) {
      tail = lastBuffer.slice(this.messageEnd + 1);
    }
    this.reset();
    this.ontext(output.toString('utf8'));
    return tail;
  };
  Receiver.prototype.error = function(reason, terminate) {
    this.reset();
    this.onerror(reason, terminate);
    return this;
  };
  Receiver.prototype.reset = function(reason) {
    if (this.dead)
      return;
    this.state = EMPTY;
    this.buffers = [];
    this.messageEnd = -1;
    this.spanLength = 0;
  };
  function bufferIndex(buffer, byte) {
    for (var i = 0,
        l = buffer.length; i < l; ++i) {
      if (buffer[i] === byte)
        return i;
    }
    return -1;
  }
})(require('buffer').Buffer);
