/* */ 
var fs = require('fs');
var http = require('http');
var express = require('express');
var app = express();
app.use(express.static(__dirname + '/public'));
var server = http.createServer(app);
var BinaryServer = require('../../lib/server').BinaryServer;
var bs = BinaryServer({server: server});
bs.on('connection', function(client) {
  client.on('stream', function(stream, meta) {
    for (var id in bs.clients) {
      if (bs.clients.hasOwnProperty(id)) {
        var otherClient = bs.clients[id];
        if (otherClient != client) {
          var send = otherClient.createStream(meta);
          stream.pipe(send);
        }
      }
    }
  });
});
server.listen(9000);
console.log('HTTP and BinaryJS server started on port 9000');
