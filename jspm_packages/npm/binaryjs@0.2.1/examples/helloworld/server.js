/* */ 
var BinaryServer = require('../../lib/server').BinaryServer;
var fs = require('fs');
var server = BinaryServer({port: 9000});
server.on('connection', function(client) {
  var file = fs.createReadStream(__dirname + '/flower.png');
  client.send(file);
});
