/* */ 
(function(process) {
  var util = require('util'),
      path = require('path'),
      events = require('events'),
      vows = exports;
  vows.options = {
    Emitter: events.EventEmitter,
    reporter: require('./vows/reporters/dot-matrix'),
    matcher: /.*/,
    error: true
  };
  vows.__defineGetter__('reporter', function() {
    return vows.options.reporter;
  });
  var stylize = require('./vows/console').stylize;
  var console = vows.console = require('./vows/console');
  vows.inspect = require('./vows/console').inspect;
  vows.prepare = require('./vows/extras').prepare;
  vows.tryEnd = require('./vows/suite').tryEnd;
  require('./assert/error');
  require('./assert/macros');
  var Suite = require('./vows/suite').Suite;
  function addVow(vow) {
    var batch = vow.batch,
        event = vow.binding.context.event || 'success',
        self = this;
    batch.total++;
    batch.vows.push(vow);
    this.on(event, function() {
      var args = Array.prototype.slice.call(arguments);
      if (!(this.ctx && this.ctx.isEvent) && vow.callback.length >= 2 && batch.suite.options.error) {
        args.unshift(null);
      }
      runTest(args, this.ctx);
      vows.tryEnd(batch);
    });
    if (event !== 'error') {
      this.on("error", function(err) {
        if (vow.callback.length >= 2 || !batch.suite.options.error) {
          runTest(arguments, this.ctx);
        } else {
          output('errored', {
            type: 'promise',
            error: err.stack || err.message || JSON.stringify(err)
          });
        }
        vows.tryEnd(batch);
      });
    }
    if (this._vowsEmitedEvents && this._vowsEmitedEvents.hasOwnProperty(event)) {
      if (Array.isArray(this._vowsEmitedEvents[event])) {
        self._vowsEmitedEvents[event].forEach(function(args) {
          runTest(args, self.ctx);
        });
      } else {
        throw new Error('_vowsEmitedEvents[' + event + '] is not an Array');
      }
      vows.tryEnd(batch);
    }
    return this;
    function runTest(args, ctx) {
      if (vow.callback instanceof String) {
        return output('pending');
      }
      if (vow.binding.context.isEvent && vow.binding.context.after) {
        var after = vow.binding.context.after;
        if (self._vowsEmitedEventsOrder.indexOf(after) > self._vowsEmitedEventsOrder.indexOf(event)) {
          output('broken', event + ' emitted before ' + after);
          return;
        }
      }
      try {
        vow.callback.apply(ctx === global || !ctx ? vow.binding : ctx, args);
        output('honored');
      } catch (e) {
        if (e.name && e.name.match(/AssertionError/)) {
          output('broken', e.toString().replace(/\`/g, '`'));
        } else {
          output('errored', e.stack || e.message || e);
        }
      }
    }
    function output(status, exception) {
      batch[status]++;
      vow.status = status;
      if (vow.context && batch.lastContext !== vow.context) {
        batch.lastContext = vow.context;
        batch.suite.report(['context', vow.context]);
      }
      batch.suite.report(['vow', {
        title: vow.description,
        context: vow.context,
        status: status,
        exception: exception || null
      }]);
    }
  }
  ;
  process.on('exit', function() {
    var results = {
      honored: 0,
      broken: 0,
      errored: 0,
      pending: 0,
      total: 0
    },
        failure;
    vows.suites.forEach(function(s) {
      if ((s.results.total > 0) && (s.results.time === null)) {
        s.reporter.print('\n\n');
        s.reporter.report(['error', {
          error: "Asynchronous Error",
          suite: s
        }]);
      }
      s.batches.forEach(function(b) {
        var unFired = [];
        b.vows.forEach(function(vow) {
          if (!vow.status) {
            if (unFired.indexOf(vow.context) === -1) {
              unFired.push(vow.context);
            }
          }
        });
        if (unFired.length > 0) {
          util.print('\n');
        }
        unFired.forEach(function(title) {
          s.reporter.report(['error', {
            error: "callback not fired",
            context: title,
            batch: b,
            suite: s
          }]);
        });
        if (b.status === 'begin') {
          failure = true;
          results.errored++;
          results.total++;
        }
        Object.keys(results).forEach(function(k) {
          results[k] += b[k];
        });
      });
    });
    if (failure) {
      util.puts(console.result(results));
    }
  });
  vows.suites = [];
  var oldEmit = vows.options.Emitter.prototype.emit;
  vows.describe = function(subject) {
    var suite = new (Suite)(subject);
    this.options.Emitter.prototype.addVow = addVow;
    this.options.Emitter.prototype.emit = function(event) {
      this._vowsEmitedEvents = this._vowsEmitedEvents || {};
      this._vowsEmitedEventsOrder = this._vowsEmitedEventsOrder || [];
      var args = Array.prototype.slice.call(arguments, 1);
      if (this._vowsEmitedEvents.hasOwnProperty(event)) {
        this._vowsEmitedEvents[event].push(args);
      } else {
        this._vowsEmitedEvents[event] = [args];
      }
      this._vowsEmitedEventsOrder.push(event);
      return oldEmit.apply(this, arguments);
    };
    this.suites.push(suite);
    if (arguments.length > 1) {
      for (var i = 1,
          l = arguments.length; i < l; ++i) {
        suite.addBatch(arguments[i]);
      }
    }
    return suite;
  };
  vows.version = JSON.parse(require('fs').readFileSync(path.join(__dirname, '..', 'package.json'))).version;
})(require('process'));
