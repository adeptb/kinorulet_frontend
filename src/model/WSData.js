export class WSData{

  static videoType = 'video';
  static audioType = 'audio';

    constructor(data,type){
      this.data = data;
      this.type = type;
    }

    toJSON(){
      return {data:this.data,type:this.type}
    }

    toJSONString(){
      JSON.stringify(this.toJSON());
    }
}
