/* */ 
var http = require('http');
var sockjs = require('../../index');
var Hapi = require('hapi');
var sockjs_opts = {sockjs_url: "http://cdn.jsdelivr.net/sockjs/1.0.1/sockjs.min.js"};
var sockjs_echo = sockjs.createServer(sockjs_opts);
sockjs_echo.on('connection', function(conn) {
  conn.on('data', function(message) {
    conn.write(message);
  });
});
var hapi_server = Hapi.createServer('0.0.0.0', 9999);
hapi_server.route({
  method: 'GET',
  path: '/{path*}',
  handler: {directory: {
      path: './html',
      listing: false,
      index: true
    }}
});
sockjs_echo.installHandlers(hapi_server.listener, {prefix: '/echo'});
console.log(' [*] Listening on 0.0.0.0:9999');
hapi_server.start();
